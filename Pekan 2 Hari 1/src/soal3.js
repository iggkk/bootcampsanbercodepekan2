let newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

let { firstName, lastName, destination, occupation, spell } = newObject;

console.log(firstName);
console.log(lastName);
console.log(destination);
console.log(occupation);
console.log(spell);